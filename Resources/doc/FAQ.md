FAQ
===

Less commonly known current limitations of the BC Hourly Invoice solution

## Commands

* bc:hi:di:dump

* bc:hi:di:create

Currently we only support one command to directly create a single invoice.

This also is limited in the reality that the invoice information is not stored in a intermediary database.


Also see [TODO.md](TODO.md) for further expansion plans

