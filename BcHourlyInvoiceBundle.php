<?php
/**
 * File containing the BcHourlyInvoiceBundle class part of the BcHourlyInvoiceBundle package.
 *
 * @copyright Copyright (C) Brookins Consulting. All rights reserved.
 * @license For full copyright and license information view LICENSE and COPYRIGHT.md file distributed with this source code.
 * @version //autogentag//
 */

namespace BrookinsConsulting\BcHourlyInvoiceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BcHourlyInvoiceBundle extends Bundle
{
    /**
     * @var string
     */
    protected $name = "BcHourlyInvoiceBundle";

    /**
     * Enable Bundle Inheritance (of the eZ Systems, eZ Publish Platform / eZ Platform, DemoBundle)
     *
     * @return string
     */
    /*public function getParent()
    {
        return 'EzPublishCoreBundle';
    }*/
}
