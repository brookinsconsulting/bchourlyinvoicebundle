BC Hourly Invoice
===================

This bundle implements a solution to provide Symfony commands to create an invoice from shell input and use a invoice template to populate the end customer invoice.


Version
=======

* The current version of BC Hourly Invoice is 0.1.0

* Last Major update: January 17, 2017


Copyright
=========

* BC Hourly Invoice is copyright 1999 - 2017 Brookins Consulting

* See: [COPYRIGHT.md](COPYRIGHT.md) for more information on the terms of the copyright and license


License
=======

BC Hourly Invoice is licensed under the GNU Affero General Public License.

The complete license agreement is included in the [LICENSE](LICENSE) file.

BC Hourly Invoice is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

BC Document Reader is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with BC Hourly Invoice in LICENSE.  If not, see <http://www.gnu.org/licenses/>.

The GNU AGPL gives you the right to use, modify and redistribute
BC Hourly Invoice under certain conditions. The GNU AGPL license
is distributed with the software, see the file LICENSE.

It is also available at [https://www.gnu.org/licenses/agpl.txt](https://www.gnu.org/licenses/agpl.txt)

Using BC Hourly Invoice under the terms of the GNU GPL is free (as in freedom).

For more information or questions please contact: license@brookinsconsulting.com


Requirements
============

The following requirements exists for using BC Hourly Invoice extension:


### Symfony version

* Make sure you use Symfony version 2.8+ (required) or higher.

* Designed and tested with Symfony 3.1.x


### PHP version

* Make sure you have PHP 7.x or higher.


Features
========

### Commands

This solution provides the following symfony commands

* Command: `bc:hi:di:create`

* Command: `bc:hi:di:dump`

### Services

* Services to assist in the generation of Hourly Invoice Documents

### Dependencies

* This solution does not depend on eZ Platform in any way


Use case requirements
=====================

This solution was created to provide for a common set of customer time card invoicing needs.

We often document our daily consulting time in 15min (industry standard) increments within a plain text file.

This solution aims to simplify this accounting through a simple to use command line command.


Installation
============

### Bundle Installation via Composer

Run the following command from your project root to install the bundle:

    bash$ composer require brookinsconsulting/bchourlyinvoicebundle dev-master;


### Bundle Activation

Within file `app/AppKernel.php` method `registerBundles` add the following into the `$bundles = array(` variable definition.

    // Brookins Consulting : BcHourlyInvoiceBundle Requirements
    new BrookinsConsulting\BcHourlyInvoiceBundle\BcHourlyInvoiceBundle(),


### Var Installation

Within symfony project root directory run the following commands.

    cd var;
    ln -s ../vendor/brookinsconsulting/bchourlyinvoicebundle/Resources/var/bchourlyinvoice .;

This creates a var directory sub directory strucuture for the symfony commands to use to access the invoice template and store the generated invoices.

### Clear the caches

Clear eZ Publish Platform / eZ platfrom caches (Required).

    php bin/console cache:clear;


Parameter / Invoice Customization
===================================

## Supported File Extensions and Mimetypes

Please review `Resources/config/hourlyinvoice.yml` and `Resources/config/services.yml` for the default invoice document types supported.

If you wish to support further file formats and invoice templates simply add them to the yaml settings and clear all caches (required).

Note: At the time of publication only PDF invoices are currently supported.

Usage
=====

The solution is configured to work virtually by default once properly installed.

### Running the dump command

To dump the form field variables supported by your invoice template simply run

    php bin/console bc:hi:di:create;
    
### Running the create command

To create a new invoice from your invoice template simply run

    php bin/console bc:hi:di:create;

#### Running the create command with shell arguments

For repeated usage of the same input you can generate invoices more quickly using shell arguments. Simply run

    php bin/console -vvv bc:hi:di:create --invoiceCustomerName="Manpow" --invoiceFileNameExtension="pdf" --invoiceStorageDirectory="var/bchourlyinvoice/generated/" --invoiceFileNameDateTimeStamp="true" --invoiceFlatten="true" --invoiceLineItemDate="01/01/2017" --invoiceCustomerAddressField1="1201 South Sherman St Ste 212" --invoiceCustomerAddressCity="Richerson" --invoiceCustomerAddressCountry="USA" --invoiceCustomerAddressRegion="Texas" --invoiceCustomerAddressPostalCode="75081" --invoiceLineItemDescription="Development of Invoice Creator" --invoiceLineItemPrice="75.00" --invoiceLineItemHours="10" --invoiceCustomerIdentifier="Manpow" --invoiceComment="Payment Due Upon Reciept" --invoiceFileName="bc_invoice_" --invoiceLineItemDeveloper="Brookins Consulting" --invoiceNumber="00001" --invoiceTaxPercentage="0" --invoiceMonetaryLocale="en_US"


Testing
=====

The solution is configured to work once properly installed and configured.

Note: At the time of writing xss testing has not been implemented nor proper unit testing.


Troubleshooting
===============

### Read the FAQ

Some problems are more common than others. The most common ones are listed in the the [Resources/doc/FAQ.md](Resources/doc/FAQ.md)


### Support

If you have find any problems not handled by this document or the FAQ you can contact Brookins Consulting through the support system: [http://brookinsconsulting.com/contact](http://brookinsconsulting.com/contact)

