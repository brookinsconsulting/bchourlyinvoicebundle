<?php
/**
 * File containing the DocumentInvoiceType class part of the BcHourlyInvoiceBundle package.
 *
 * @copyright Copyright (C) Brookins Consulting. All rights reserved.
 * @license For full copyright and license information view LICENSE and COPYRIGHT.md file distributed with this source code.
 * @version //autogentag//
 */

namespace BrookinsConsulting\BcHourlyInvoiceBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentInvoiceType
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $displayDebug;

    /**
     * @var int
     */
    protected $displayDebugLevel;

    /**
     * @var array
     */
    protected $handlers = null;

    /**
     * @var string
     */
    protected $handlerKey = null;

    /**
     * @var string
     */
    protected $handlerClassName = null;

    /**
     * @var string
     */
    protected $templateDocumentFileNamePath;

    /**
     * @var array
     */
    protected $values = null;

    /**
     * @var int
     */
    protected $randomSeedStart;

    /**
     * @var int
     */
    protected $randomSeedEnd;

    /**
     * @var array
     */
    protected $fieldNames = array(
        array('Developer', 'developer'),
        array('Description', 'description'),
        array('Price', 'hourly_rate'),
        array('Hours', 'hours'),
        array('TotalCostDisplay', 'total_price'),
    );

    /**
     * Constructor
     *
     * @param Symfony\Component\DependencyInjection\ContainerInterface $container
     * @param array $options
     */
    public function __construct(ContainerInterface $container, array $options = array())
    {
        $this->container = $container;
        $this->options = $options[0]['options'];
        $this->handlers = $options[0]['handlers'];
        $this->values = $options[0]['values'];
        $this->displayDebug = $this->options['display_debug'] == true ? true : false;
        $this->displayDebugLevel = is_numeric(
            $this->options['display_debug_level']
        ) ? $this->options['display_debug_level'] : 0;
        $this->handlerKey = $this->options['default_handler'];
        $this->templateDocumentFileNamePath = $this->options['default_template'];
        $this->randomSeedStart = $this->values['random_range_start'];
        $this->randomSeedEnd = $this->values['random_range_end'];
    }

    /**
     * Get handlers array
     *
     * @return array $this->handlers
     */
    public function getHandlers()
    {
        return (array)$this->handlers;
    }

    /**
     * Get handler type object
     *
     * @param string $argHandlerKey Handler Class
     * @return array $this->handlers
     */
    public function handler($argHandlerKey = null)
    {
        if ($argHandlerKey != null) {
            $this->handlerKey = $argHandlerKey;
        }

        $documentCreated = $this->container->get(
            'brookinsconsulting.hourly_invoice.document_invoice_type_'.ucfirst($this->handlerKey)
        );

        return $documentCreated;
    }

    /**
     * Calculate display values document in Pdf
     *
     * @param array $values Values to be inserted into pdf template
     * @return array $values Returns array of data and return value true if document is created successfully
     */
    public function calculate(array $values = array())
    {
        if (array_key_exists('invoiceFileName', $values)) {
            $invoiceFileName = $values['invoiceFileName'];
            $invoiceFileNameDateTimeStamp = $values['invoiceFileNameDateTimeStamp'];
            $invoiceFileNameDateTimeStampFormatted = null;
            $invoiceFileNameUniqueIdentifier = random_int($this->randomSeedStart, $this->randomSeedEnd);
            if ($invoiceFileNameDateTimeStamp === "true") {
                $invoiceFileNameDateTimeStampFormatted = date('Y_m_d--H-i-s__');
            }
            $invoiceCustomerAddressDisplay = $values['invoiceCustomerName']."\n".
                $values['invoiceCustomerAddressField1']."\n";

            if (array_key_exists(
                    'invoiceCustomerAddressField2',
                    $values
                ) && $values['invoiceCustomerAddressField2'] != null
            ) {
                $invoiceCustomerAddressDisplay .= $values['invoiceCustomerAddressField2']."\n";
            }

            if (array_key_exists(
                    'invoiceCustomerAddressField3',
                    $values
                ) && $values['invoiceCustomerAddressField3'] != null
            ) {
                $invoiceCustomerAddressDisplay .= $values['invoiceCustomerAddressField3']."\n";
            }

            $invoiceCustomerAddressDisplay .= $values['invoiceCustomerAddressCity'].", ".
                $values['invoiceCustomerAddressRegion']." ".$values['invoiceCustomerAddressPostalCode'].' '.
                $values['invoiceCustomerAddressCountry'];

            $values['invoiceCustomerAddressDisplay'] = $invoiceCustomerAddressDisplay;

            // Iterate over invoice line items dynamically
            $fieldNamesIndex = 0;
            $lineItemsSubTotal = 0;
            $i = 0;
            $iNext = 1;
            $iEnd = 4;

            while ($i <= $iEnd) {
                if ($i <= 0) {
                    $iAppend = '';
                } else {
                    $iAppend = $i + 1;
                }

                if (array_key_exists($fieldNamesIndex, $this->fieldNames) && array_key_exists(
                        'invoiceLineItem'.
                        $this->fieldNames[$fieldNamesIndex][0].$iAppend,
                        $values
                    )
                ) {
                    $lineItemTotalCost = $values['invoiceLineItem'.$this->fieldNames[2][0].$iAppend] * $values['invoiceLineItem'.$this->fieldNames[3][0].$iAppend];
                    $values['invoiceLineItem'.$this->fieldNames[2][0].$iAppend] = money_format(
                        '$%i',
                        $values['invoiceLineItem'.$this->fieldNames[2][0].$iAppend]
                    );
                    $lineItemTotalCostDisplay = money_format('$%i', $lineItemTotalCost);
                    $values['invoiceLineItem'.$this->fieldNames[4][0].$iAppend] = $lineItemTotalCostDisplay;
                    $lineItemsSubTotal += $lineItemTotalCost;
                }

                if ($i == $iEnd) {
                    $lineItemsSubTotalDisplay = money_format('$%i', $lineItemsSubTotal);
                    $values['invoiceLineItemsSubTotalDisplay'] = $lineItemsSubTotalDisplay;

                    if ($values['invoiceTaxPercentage'] === "0") {
                        $totalTax = $values['invoiceTaxPercentage'];
                        $totalCostPlusTax = $lineItemsSubTotal;
                    } else {
                        $totalTax = $lineItemsSubTotal * $values['invoiceTaxPercentage'] / 100;
                        $totalCostPlusTax = $lineItemsSubTotal + $totalTax;
                        //$totalTaxPercentage = (($totalCostPlusTax-$lineItemsSubTotal)/$lineItemsSubTotal)*100;
                    }

                    $invoiceTaxPercentageDisplay = $totalTax.' %';
                    $values['invoiceTaxPercentageDisplay'] = $invoiceTaxPercentageDisplay;
                    $totalCostDisplay = money_format('$%i', $totalCostPlusTax);
                    $values['totalCostDisplay'] = $totalCostDisplay;
                    $fieldNamesIndex = -1;
                }

                $i++;
                $iNext++;
                $fieldNamesIndex++;
            }

            $invoiceFileNameExtension = $values['invoiceFileNameExtension'];
            $invoiceStorageDirectory = $values['invoiceStorageDirectory'];
            $outputFileNamePath = __DIR__.'/../../../../'.$invoiceStorageDirectory.$invoiceFileName.$invoiceFileNameDateTimeStampFormatted.$invoiceFileNameUniqueIdentifier.'.'.$invoiceFileNameExtension;

            $values['outputFileNamePath'] = $outputFileNamePath;
        }

        $templateFileNamePath = __DIR__.'/../../../../'.$this->templateDocumentFileNamePath;
        $values['templateFileNamePath'] = $templateFileNamePath;

        return $values;
    }
}