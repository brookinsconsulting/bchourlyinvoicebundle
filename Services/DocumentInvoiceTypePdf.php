<?php
/**
 * File containing the DocumentInvoiceTypePdf class part of the BcHourlyInvoiceBundle package.
 *
 * @copyright Copyright (C) Brookins Consulting. All rights reserved.
 * @copyleft Nikolay Kitsul
 * @license For full copyright and license information view LICENSE and COPYRIGHT.md file distributed with this source code.
 * @version //autogentag//
 * @note Use proper escaping functions: pdfff_escape($str), pdfff_checkbox($value)
 */

namespace BrookinsConsulting\BcHourlyInvoiceBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use mikehaertl\pdftk\Pdf;

class DocumentInvoiceTypePdf extends DocumentInvoiceType
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $displayDebug;

    /**
     * @var int
     */
    protected $displayDebugLevel;

    /**
     * @var string
     */
    protected $handlerClassName = null;

    /**
     * @var string
     */
    protected $templateDocumentFileNamePath;

    /**
     * @var array
     */
    protected $values = null;

    /**
     * @var int
     */
    protected $randomSeedStart;

    /**
     * @var int
     */
    protected $randomSeedEnd;

    /**
     * Constructor
     *
     * @param Symfony\Component\DependencyInjection\ContainerInterface $container
     * @param array $options
     */
    public function __construct(ContainerInterface $container, array $options = array())
    {
        $this->container = $container;
        $this->options = $options[0]['options'];
        $this->values = $options[0]['values'];
        $this->displayDebug = $this->options['display_debug'] == true ? true : false;
        $this->displayDebugLevel = is_numeric(
            $this->options['display_debug_level']
        ) ? $this->options['display_debug_level'] : 0;
        $this->templateDocumentFileNamePath = $this->options['default_template'];
        $this->randomSeedStart = $this->values['random_range_start'];
        $this->randomSeedEnd = $this->values['random_range_end'];
    }

    /**
     * Create document in Pdf
     *
     * @param array $values Values to be inserted into pdf template
     * @return mixed $documentCreated Returns array of data and return value true if document is created successfully
     */
    public function create(array $values = array())
    {
        $documentCreated = false;

        // Get form data field display values
        $values = $this->calculate($values);

        // Get the pdf template and output file path
        $templateFileNamePath = $values['templateFileNamePath'];
        $outputFileNamePath = $values['outputFileNamePath'];

        // Get pdf object for template file name path
        $pdf = new Pdf($templateFileNamePath);

        // Create field map of form field data
        $fieldMapping = array(
            'date' => $values['invoiceLineItemDate'],
            'invoice_number' => $values['invoiceNumber'],
            'customer_id' => $values['invoiceCustomerIdentifier'],
            'bill_to' => $values['invoiceCustomerAddressDisplay'],
        );

        // Iterate over invoice line items dynamically
        $fieldNames = array(
            array('Developer', 'developer'),
            array('Description', 'description'),
            array('Price', 'hourly_rate'),
            array('Hours', 'hours'),
            array('TotalCostDisplay', 'total_price'),
        );
        $fieldNamesIndex = 0;
        $i = 0;
        $iNext = 1;
        $iEnd = 4;

        while ($i <= $iEnd) {
            if ($i <= 0) {
                $iAppend = '';
                $iUnderscoreAppend = '_'.($i + 1);
            } else {
                $iAppend = $i + 1;
                $iUnderscoreAppend = "_".$iAppend;
            }

            if (array_key_exists($fieldNamesIndex, $fieldNames) && array_key_exists(
                    'invoiceLineItem'.
                    $fieldNames[$fieldNamesIndex][0].$iAppend,
                    $values
                )
            ) {
                $fieldMapping[$fieldNames[0][1].$iUnderscoreAppend] = $values['invoiceLineItem'.$fieldNames[0][0].$iAppend];
                $fieldMapping[$fieldNames[1][1].$iUnderscoreAppend] = $values['invoiceLineItem'.$fieldNames[1][0].$iAppend];
                $fieldMapping[$fieldNames[2][1].$iUnderscoreAppend] = $values['invoiceLineItem'.$fieldNames[2][0].$iAppend];
                $fieldMapping[$fieldNames[3][1].$iUnderscoreAppend] = $values['invoiceLineItem'.$fieldNames[3][0].$iAppend];
                $fieldMapping[$fieldNames[4][1].$iUnderscoreAppend] = $values['invoiceLineItem'.$fieldNames[4][0].$iAppend];
            }

            if ($i == $iEnd) {
                $fieldMapping['subtotal'] = $values['invoiceLineItemsSubTotalDisplay'];
                $fieldMapping['total'] = $values['totalCostDisplay'];
                $fieldMapping['tax'] = $values['invoiceTaxPercentageDisplay'];
                $fieldNamesIndex = -1;
            }

            $i++;
            $iNext++;
            $fieldNamesIndex++;
        }

        // Assign the invoice comments
        $fieldMapping['comments'] = $values['invoiceComment'];

        // Populate form fields with data
        if ($values['invoiceFlatten']) {
            $documentCreated = $pdf->fillForm($fieldMapping)
                ->needAppearances()
                ->flatten()
                ->saveAs($outputFileNamePath);
        } else {
            $documentCreated = $pdf->fillForm($fieldMapping)
                ->needAppearances()
                ->saveAs($outputFileNamePath);
        }

        // Test for document creation and success to build return variable
        if (file_exists($outputFileNamePath) && $documentCreated === true) {
            $documentCreated = array($outputFileNamePath, $documentCreated);
        }

        // Check for errors
        if (!$documentCreated) {
            $error = $pdf->getError();
            $documentCreated = array($outputFileNamePath, $documentCreated, $error);
        }

        return $documentCreated;
    }

    /**
     * Dump pdf template document fields
     *
     * @param array $values Values to be inserted into pdf template
     * @param string $templateFileNamePath File name path to pdf template file
     * @return mixed $documentFieldsDumped Returns array of data and return value true if document fields are extracted successfully
     */
    public function dumpDocumentFieldNames($templateFileNamePath = null)
    {
        $documentFieldsDumped = false;

        // Get form data fields
        $pdf = new Pdf($templateFileNamePath);
        $data = $pdf->getDataFields();

        // Test returned results for completion
        if (is_string($data)) {
            $documentFieldsDumped = array($data, true);
        }

        return $documentFieldsDumped;
    }
}