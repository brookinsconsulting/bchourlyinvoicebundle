<?php
/**
 * File containing the DocumentInvoiceCommand class part of the BcHourlyInvoiceBundle package.
 *
 * @copyright Copyright (C) Brookins Consulting. All rights reserved.
 * @license For full copyright and license information view LICENSE and COPYRIGHT.md file distributed with this source code.
 * @version //autogentag//
 */

namespace BrookinsConsulting\BcHourlyInvoiceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class DocumentInvoiceCommand extends ContainerAwareCommand
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $displayDebug;

    /**
     * @var int
     */
    protected $displayDebugLevel;

    /**
     * @var array
     */
    protected $questionAnswers = array();

    /**
     * @var object
     */
    protected $helper;

    /**
     * Configure hourly invoice document creation command line options
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('bc:hi:di:create')
            ->setDescription('Directly Create Invoice Document')
            ->addOption(
                'invoiceCustomerName',
                null,
                InputOption::VALUE_REQUIRED,
                'The name of the customer'
            )
            ->addOption(
                'invoiceCustomerAddressField1',
                null,
                InputOption::VALUE_REQUIRED,
                'The first address field content'
            )
            // Consider Adding support for Address Fields Four and Five (Prison Issue)
            ->addOption(
                'invoiceCustomerAddressCity',
                null,
                InputOption::VALUE_REQUIRED,
                'The city name of the customer address'
            )
            ->addOption(
                'invoiceCustomerAddressCountry',
                null,
                InputOption::VALUE_REQUIRED,
                'The country of the customer address'
            )
            ->addOption(
                'invoiceCustomerAddressRegion',
                null,
                InputOption::VALUE_REQUIRED,
                'The region / state of the customer address'
            )
            ->addOption(
                'invoiceCustomerAddressPostalCode',
                null,
                InputOption::VALUE_REQUIRED,
                'The postal / zip code of the customer address'
            )
            ->addOption(
                'invoiceLineItemDescription',
                null,
                InputOption::VALUE_REQUIRED,
                'The description of the invoice line item'
            )
            ->addOption(
                'invoiceLineItemDeveloper',
                null,
                InputOption::VALUE_REQUIRED,
                'The developer of the invoice line item'
            )
            ->addOption(
                'invoiceLineItemPrice',
                null,
                InputOption::VALUE_REQUIRED,
                'The price of the invoice line item'
            )
            ->addOption(
                'invoiceLineItemHours',
                null,
                InputOption::VALUE_REQUIRED,
                'The number of hours of the invoice line item'
            )
            ->addOption(
                'invoiceFileNameDateTimeStamp',
                null,
                InputOption::VALUE_OPTIONAL,
                'Append a timestamp to the invoice file name'
            )
            ->addOption(
                'invoiceCustomerAddressField2',
                null,
                InputOption::VALUE_OPTIONAL,
                'The second address field content'
            )
            ->addOption(
                'invoiceCustomerAddressField3',
                null,
                InputOption::VALUE_OPTIONAL,
                'The third address field content'
            )
            ->addOption(
                'invoiceLineItemDate',
                null,
                InputOption::VALUE_OPTIONAL,
                'The date of the invoice line item. Defaults to current date time.'
            )
            ->addOption(
                'invoiceTaxPercentage',
                null,
                InputOption::VALUE_OPTIONAL,
                'The percentage of tax for the invoice total'
            )
            ->addOption(
                'invoiceCustomerIdentifier',
                null,
                InputOption::VALUE_OPTIONAL,
                'The text or numeric identifier of the customer'
            )
            ->addOption(
                'invoiceComment',
                null,
                InputOption::VALUE_OPTIONAL,
                'The text comment description'
            )
            ->addOption(
                'invoiceNumber',
                null,
                InputOption::VALUE_OPTIONAL,
                'The invoice number of the invoice document to be created'
            )
            ->addOption(
                'invoiceFileName',
                null,
                InputOption::VALUE_OPTIONAL,
                'The file name of the invoice document to be created. Defaults to "bc_invoice_"'
            )
            ->addOption(
                'invoiceFileNameExtension',
                null,
                InputOption::VALUE_OPTIONAL,
                'The invoice file name extension of the invoice document to be created. Defaults to "pdf".'
            )
            ->addOption(
                'invoiceStorageDirectory',
                null,
                InputOption::VALUE_OPTIONAL,
                'The invoice template storage directory. Defaults to "var/template/bchourlyinvoice".'
            )
            ->addOption(
                'invoiceFileNameDateTimestamp',
                null,
                InputOption::VALUE_OPTIONAL,
                'The invoice filename includes a date time stamp to ensure the filename is unique. Defaults to true.'
            )
            ->addOption(
                'invoiceMonetaryLocale',
                null,
                InputOption::VALUE_OPTIONAL,
                'The invoice monetary / currency locale. Defaults to [en_US].'
            )
            ->addOption(
                'invoiceFlatten',
                null,
                InputOption::VALUE_OPTIONAL,
                'Flatten the invoice document. Defaults to true.'
            );
    }

    /**
     * Execute command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {

        // Ask document creation requirement questions
        $this->helper = $this->getHelper('question');
        $this->optionsQuestions($input, $output);
        $this->confirmQuestion($input, $output);

        // Create hourly invoice document
        $this->create($input, $output);
    }

    /**
     * Ask Options Questions
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function optionsQuestions(
        InputInterface $input,
        OutputInterface $output
    ) {

        // Access helper object
        $helper = $this->helper;

        // Get Option Input and Ask Remaining Option Questions

        if ($input->getOption('invoiceMonetaryLocale') === null) {
            // Ask: Invoice Document Monetary Locale Option Question
            $qMonetaryLocale = new Question(
                "What is the customer invoice monetary locale? (Defaults to [en_US])", 'en_US'
            );

            $aMonetaryLocale = $helper->ask($input, $output, $qMonetaryLocale);

            if ($aMonetaryLocale) {
                setlocale(LC_MONETARY, $aMonetaryLocale);
                $this->questionAnswers['invoiceMonetaryLocale'] = $aMonetaryLocale;
            }
        }

        if ($input->getOption('invoiceLineItemDate') === null) {
            // Ask: Invoice Document Date Option Question
            $qInvoiceLineItemDate = new Question(
                "What is the customer invoice date? (In any format. Defaults to today)", date('m/d/Y')
            );

            $aInvoiceLineItemDate = $helper->ask($input, $output, $qInvoiceLineItemDate);

            if ($aInvoiceLineItemDate) {
                $this->questionAnswers['invoiceLineItemDate'] = $aInvoiceLineItemDate;
            }
        }

        if ($input->getOption('invoiceCustomerName') === null) {
            // Ask: Invoice Document Customer Name Option Question
            $qInvoiceCustomerName = new Question(
                "What is the customer invoice name? (Defaults to empty)", ''
            );

            $aInvoiceCustomerName = $helper->ask($input, $output, $qInvoiceCustomerName);

            if ($aInvoiceCustomerName) {
                $this->questionAnswers['invoiceCustomerName'] = $aInvoiceCustomerName;
            }
        }

        if ($input->getOption('invoiceNumber') === null) {
            // Ask: Invoice Document FileName Extension Option Question
            $qInvoiceNumber = new Question(
                "What is the customer invoice number? ([0000001])", '0000001'
            );

            $aInvoiceNumber = $helper->ask($input, $output, $qInvoiceNumber);

            if ($aInvoiceNumber) {
                $this->questionAnswers['invoiceNumber'] = $aInvoiceNumber;
            }
        }

        if (!array_key_exists('invoiceCustomerAddressFields', $input->getOptions())) {
            $aInvoiceLineItemAddressFieldPrompt = true;
            $invoiceLineItemAddressFieldLimit = 5;

            // Ask: Invoice Document Address Fields Option Questions
            if ($input->getOption('invoiceCustomerAddressField1')) {
                $i = 2;
                $endI = 5;
                $invoiceLineItemCount = 2;
            } else {
                $i = 1;
                $endI = 5;
                $invoiceLineItemCount = 1;
            }

            while ($i <= $invoiceLineItemAddressFieldLimit) {
                //echo "$i - $invoiceLineItemCount - $invoiceLineItemAddressFieldLimit ( $i >= 1) \n";

                if ($aInvoiceLineItemAddressFieldPrompt === true) {
                    ${"qInvoiceLineItemAddressFieldPrompt"} = new ConfirmationQuestion(
                        "Would you like to add additional invoice address line? ($i-5, Not required. [No]/Yes)", false
                    );

                    ${"aInvoiceLineItemAddressFieldPrompt"} = $helper->ask(
                        $input,
                        $output,
                        ${"qInvoiceLineItemAddressFieldPrompt"}
                    );
                    if (${"aInvoiceLineItemAddressFieldPrompt"} === false) {
                        $i = 6;
                    }
                }

                while ($i <= $endI && ${"aInvoiceLineItemAddressFieldPrompt"} === true) {
                    ${"qInvoiceCustomerAddressField".$i} = new Question(
                        "What is the customer address field $i content? (Not required. Defaults to empty)", ''
                    );

                    ${"aInvoiceCustomerAddressField".$i} = $helper->ask(
                        $input,
                        $output,
                        ${"qInvoiceCustomerAddressField".$i}
                    );

                    if (${"aInvoiceCustomerAddressField".$i}) {
                        $this->questionAnswers["invoiceCustomerAddressField$i"] = ${"aInvoiceCustomerAddressField".$i};
                    }
                    if ($invoiceLineItemCount < 3) {
                        $invoiceLineItemCount++;
                    }
                    break 1;
                }

                $i++;
            }
        }

        if ($input->getOption('invoiceCustomerAddressCity') === null) {
            // Ask: Invoice Document Customer Address City Option Question
            $qInvoiceCustomerAddressCity = new Question(
                "What is the customer address city field content? (Defaults to empty)", ''
            );

            $aInvoiceCustomerAddressCity = $helper->ask($input, $output, $qInvoiceCustomerAddressCity);

            if ($aInvoiceCustomerAddressCity) {
                $this->questionAnswers['invoiceCustomerAddressCity'] = $aInvoiceCustomerAddressCity;
            }
        }

        if ($input->getOption('invoiceCustomerAddressCountry') === null) {
            // Ask: Invoice Document Customer Address Country Option Question
            $qInvoiceCustomerAddressCountry = new Question(
                "What is the customer second address country field content? (Defaults to empty)", ''
            );

            $aInvoiceCustomerAddressCountry = $helper->ask($input, $output, $qInvoiceCustomerAddressCountry);

            if ($aInvoiceCustomerAddressCountry) {
                $this->questionAnswers['invoiceCustomerAddressCountry'] = $aInvoiceCustomerAddressCountry;
            }
        }

        if ($input->getOption('invoiceCustomerAddressRegion') === null) {
            // Ask: Invoice Document Customer Address Region or State Option Question
            $qInvoiceCustomerAddressRegion = new Question(
                "What is the customer address region or state field content? (Defaults to empty)", ''
            );

            $aInvoiceCustomerAddressRegion = $helper->ask($input, $output, $qInvoiceCustomerAddressRegion);

            if ($aInvoiceCustomerAddressRegion) {
                $this->questionAnswers['invoiceCustomerAddressRegion'] = $aInvoiceCustomerAddressRegion;
            }
        }

        if ($input->getOption('invoiceCustomerAddressPostalCode') === null) {
            // Ask: Invoice Document Customer Address Postal Code or Zip code Option Question
            $qInvoiceCustomerAddressPostalCode = new Question(
                "What is the customer address postal or zip code field content? (Defaults to empty)", ''
            );

            $aInvoiceCustomerAddressPostalCode = $helper->ask($input, $output, $qInvoiceCustomerAddressPostalCode);

            if ($aInvoiceCustomerAddressPostalCode) {
                $this->questionAnswers['invoiceCustomerAddressPostalCode'] = $aInvoiceCustomerAddressPostalCode;
            }
        }

        if ($input->getOption('invoiceTaxPercentage') === null) {
            // Ask: Invoice Document Tax Percentage Option Question
            $qInvoiceTaxPercentage = new Question(
                "What is the customer invoice tax percentage? (In decimal notation)", ''
            );

            $aInvoiceTaxPercentage = $helper->ask($input, $output, $qInvoiceTaxPercentage);

            if ($aInvoiceTaxPercentage) {
                $this->questionAnswers['invoiceTaxPercentage'] = $aInvoiceTaxPercentage;
            }
        }

        if ($input->getOption('invoiceCustomerIdentifier') === null) {
            // Ask: Invoice Document Customer Identifier Option Question
            $qInvoiceCustomerIdentifier = new Question(
                "What is the customer invoice customer identifier text string? (number or alpha)", ''
            );

            $aInvoiceCustomerIdentifier = $helper->ask($input, $output, $qInvoiceCustomerIdentifier);

            if ($aInvoiceCustomerIdentifier) {
                $this->questionAnswers['invoiceCustomerIdentifier'] = $aInvoiceCustomerIdentifier;
            }
        }

        if ($input->getOption('invoiceComment') === null) {
            // Ask: Invoice Document Comment Option Question
            $qInvoiceComment = new Question(
                "What is the customer invoice comment or special instructions?)", ''
            );

            $aInvoiceComment = $helper->ask($input, $output, $qInvoiceComment);

            if ($aInvoiceComment) {
                $this->questionAnswers['invoiceComment'] = $aInvoiceComment;
            }
        }

        if ($input->getOption('invoiceFileName') === null) {
            // Ask: Invoice Document FileName Extension Option Question
            $qInvoiceFileName = new Question(
                "Would you like to modify the invoice document output filename? ([bc_invoice_]/'') ", 'bc_invoice_'
            );

            $aInvoiceFileName = $helper->ask($input, $output, $qInvoiceFileName);

            if ($aInvoiceFileName) {
                $this->questionAnswers['invoiceFileName'] = $aInvoiceFileName;
            }
        }

        if ($input->getOption('invoiceFileNameExtension') === null) {
            // Ask: Invoice Document FileName Extension Option Question
            $qInvoiceFileNameExtension = new Question(
                "Would you like to modify the invoice document type? (Defaults to [pdf]) ", 'pdf'
            );

            $aInvoiceFileNameExtension = $helper->ask($input, $output, $qInvoiceFileNameExtension);

            if ($aInvoiceFileNameExtension) {
                $this->questionAnswers['invoiceFileNameExtension'] = $aInvoiceFileNameExtension;
            }
        }

        if ($input->getOption('invoiceFileNameDateTimeStamp') === null) {
            // Ask: Add Invoice FileName DateTimestamp Option Question
            $qInvoiceFileNameDateTimestamp = new Question(
                "Would you like to add a datetime stamp to the invoice document filename? ([yes]/no) ", "true"
            );

            $aInvoiceFileNameDateTimestamp = $helper->ask($input, $output, $qInvoiceFileNameDateTimestamp);

            if ($aInvoiceFileNameDateTimestamp) {
                $this->questionAnswers['invoiceFileNameDateTimeStamp'] = $aInvoiceFileNameDateTimestamp;
            }
        }

        if ($input->getOption('invoiceStorageDirectory') === null) {
            // Ask: Invoice Template Storage Directory Option Question
            $qInvoiceStorageDirectory = new Question(
                "Would you like to modify the invoice document template filename storage path? ([var/bchourlyinvoice]/generated/])",
                'var/bchourlyinvoice/generated/'
            );

            $aInvoiceStorageDirectory = $helper->ask($input, $output, $qInvoiceStorageDirectory);

            if ($aInvoiceStorageDirectory) {
                $this->questionAnswers['invoiceStorageDirectory'] = $aInvoiceStorageDirectory;
            }
        }

        if (!array_key_exists('invoiceLineItemFields', $input->getOptions())) {
            $aInvoiceLineItemsAdditionalFieldPrompt = true;
            $fieldNames = array('Developer', 'Description', 'Price', 'Hours');
            $fieldNamesIndex = 0;
            $invoiceLineItemLimit = 5;
            $endI = 6;

            // Ask: Invoice Document Line Item Fields Option Questions
            if ($input->getOption('invoiceLineItemDescription') && $input->getOption('invoiceLineItemPrice')
                && $input->getOption('invoiceLineItemHours')
            ) {
                $i = 2;
                $invoiceLineItemCount = 2;
            } else {
                $i = 1;
                $invoiceLineItemCount = 1;
            }

            while ($i <= $invoiceLineItemLimit) {

                if ($aInvoiceLineItemsAdditionalFieldPrompt === true) {
                    ${"qInvoiceLineItemsAdditionalFieldPrompt"} = new ConfirmationQuestion(
                        "Would you like to add additional invoice line items? (Not required. [No]/Yes)", false
                    );

                    ${"aInvoiceLineItemsAdditionalFieldPrompt"} = $helper->ask(
                        $input,
                        $output,
                        ${"qInvoiceLineItemsAdditionalFieldPrompt"}
                    );
                    if (${"aInvoiceLineItemsAdditionalFieldPrompt"} === false) {
                        $i = 6;
                    }
                }

                while ($i <= $endI && ${"aInvoiceLineItemsAdditionalFieldPrompt"} === true) {
                    ${"qInvoiceLineItem".$fieldNames[$fieldNamesIndex]."Field".$i} = new Question(
                        "What is the invoice line item $invoiceLineItemCount, ".$fieldNames[$fieldNamesIndex]." field content? (Not required. Defaults to empty)",
                        ''
                    );

                    ${"aInvoiceLineItem".$fieldNames[$fieldNamesIndex]."Field".$i} = $helper->ask(
                        $input,
                        $output,
                        ${"qInvoiceLineItem".$fieldNames[$fieldNamesIndex]."Field".$i}
                    );

                    if (${"aInvoiceLineItem".$fieldNames[$fieldNamesIndex]."Field".$i}) {
                        $this->questionAnswers["invoiceLineItem".$fieldNames[$fieldNamesIndex].$i] = ${"aInvoiceLineItem".$fieldNames[$fieldNamesIndex]."Field".$i};
                    }

                    if ($fieldNamesIndex < 3) {
                        $fieldNamesIndex++;
                    } else {
                        $fieldNamesIndex = 0;
                        $invoiceLineItemCount++;
                        break 1;
                    }
                }

                if ($i <= 3) {
                    $i++;
                }
            }
        }

        if ($input->getOption('invoiceFlatten') === null) {
            // Ask: Flatten Invoice Option Question
            $qInvoiceFlatten = new Question(
                "Would you like to flatten the invoice document? (yes/[no]) ", false
            );

            $aInvoiceFlatten = $helper->ask($input, $output, $qInvoiceFlatten);

            if ($aInvoiceFlatten) {
                $this->questionAnswers['invoiceFlatten'] = $aInvoiceFlatten;
            }
        }
    }

    /**
     * Ask Options Questions
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function confirmQuestion(
        InputInterface $input,
        OutputInterface $output
    ) {
        /*
        $output->writeln(
            "======[ SUMMARY ]======\n" .
            " Customer Name: " . $this->aInvoiceCustomerName . "\n" .

        );
        */
        return true;
    }

    /**
     * Create hourly invoice document
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @param array $options
     * @return bool
     */
    public function create(
        InputInterface $input,
        OutputInterface $output
    ) {

        // Get document invoice type service
        $documentInvoiceType = $this->getContainer()->get('brookinsconsulting.hourly_invoice.document_invoice_type');

        // Get options
        $values = array_merge($input->getOptions(), $this->questionAnswers);

        // Get the invoice document file extension type
        if (array_key_exists('invoiceFileNameExtension', $values)) {
            $handler = $values['invoiceFileNameExtension'];
        } else {
            $handler = 'pdf';
        }

        // Test if handler contains null
        if (strpos($handler, null)) {
            $result = $documentInvoiceType->handler()->create($values);
        } else {
            $result = $documentInvoiceType->handler($handler)->create($values);
        }

        // Display results to cli buffer
        if (is_array($result) && $result[1] === true) {
            $output->writeln("Success! Invoice Document Created At: \n".$result[0]);
            $result = true;
        } else {
            $output->writeln("Failure! Invoice Document Creation Failed!: ");
            $output->writeln(print_r($result));
        }

        return $result;
    }
}