<?php
/**
 * File containing the DocumentInvoiceDumpCommand class part of the BcHourlyInvoiceBundle package.
 *
 * @copyright Copyright (C) Brookins Consulting. All rights reserved.
 * @license For full copyright and license information view LICENSE and COPYRIGHT.md file distributed with this source code.
 * @version //autogentag//
 */

namespace BrookinsConsulting\BcHourlyInvoiceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class DocumentInvoiceDumpCommand extends ContainerAwareCommand
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $displayDebug;

    /**
     * @var int
     */
    protected $displayDebugLevel;

    /**
     * @var array
     */
    protected $questionAnswers = array();

    /**
     * @var object
     */
    protected $helper;

    /**
     * Configure hourly invoice document creation command line options
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('bc:hi:di:dump')
            ->setDescription('Directly Dump Invoice Document Form Field Variables')
            ->addOption(
                'invoiceFileNameExtension',
                null,
                InputOption::VALUE_OPTIONAL,
                'The invoice file name extension of the invoice document to be created. Defaults to "pdf".'
            );
    }

    /**
     * Execute command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {

        // Ask document creation requirement questions
        $this->helper = $this->getHelper('question');
        $this->optionsQuestions($input, $output);
        $this->confirmQuestion($input, $output);

        // Create hourly invoice document
        $this->create($input, $output);
    }

    /**
     * Ask Options Questions
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function optionsQuestions(
        InputInterface $input,
        OutputInterface $output
    ) {

        // Access helper object
        $helper = $this->helper;

        // Get Option Input and Ask Remaining Option Questions

        if ($input->getOption('invoiceFileNameExtension') === null) {
            // Ask: Invoice Document FileName Extension Option Question
            $qInvoiceFileNameExtension = new Question(
                "Would you like to modify the invoice document type? (Defaults to [pdf]) ", 'pdf'
            );

            $aInvoiceFileNameExtension = $helper->ask($input, $output, $qInvoiceFileNameExtension);

            if ($aInvoiceFileNameExtension) {
                $this->questionAnswers['invoiceFileNameExtension'] = $aInvoiceFileNameExtension;
            }
        }
    }

    /**
     * Ask Options Questions
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function confirmQuestion(
        InputInterface $input,
        OutputInterface $output
    ) {
        /*
        $output->writeln(
            "======[ SUMMARY ]======\n" .
            " Customer Name: " . $this->aInvoiceCustomerName . "\n" .

        );
        */
        return true;
    }

    /**
     * Create hourly invoice document
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @param array $options
     * @return bool
     */
    public function create(
        InputInterface $input,
        OutputInterface $output
    ) {

        // Get document invoice type service
        $documentInvoiceType = $this->getContainer()->get('brookinsconsulting.hourly_invoice.document_invoice_type');

        // Get options
        $values = array_merge($input->getOptions(), $this->questionAnswers);
        $values = $documentInvoiceType->calculate($values);

        // Get the invoice document file extension type
        if (array_key_exists('invoiceFileNameExtension', $values)) {
            $handler = $values['invoiceFileNameExtension'];
        } else {
            $handler = 'pdf';
        }

        // Test if handler contains null
        if (strpos($handler, null)) {
            // Get form data field display values
            $result = $documentInvoiceType->handler()->dumpDocumentFieldNames($values['templateFileNamePath']);
        } else {
            $result = $documentInvoiceType->handler($handler)->dumpDocumentFieldNames($values['templateFileNamePath']);
        }

        // Display results to cli buffer
        if (is_array($result) && $result[1] === true) {
            $output->writeln($result[0]);
            $output->writeln(" ");
            $output->writeln("Success! Invoice Document Dumped ");
            $result = true;
        } else {
            $output->writeln("Failure! Invoice Document Dump Failed! ");
        }

        return $result;
    }
}